package code.smsbackup;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;

import androidx.annotation.RequiresApi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SMSHelpers {

    private Context mainAct;

    SMSHelpers(MainActivity mainAct) {

        this.mainAct = mainAct;

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public List<String> getAllSms(boolean inbox) {// inbox == false if outbox
        List<String> lstSms = new ArrayList<String>();
        ContentResolver cr = this.mainAct.getContentResolver();

        Cursor cur = null;
        if (inbox) {
            cur = cr.query(Telephony.Sms.Inbox.CONTENT_URI, // Official CONTENT_URI from docs
                    new String[]{Telephony.Sms.Inbox.BODY}, // Select body text
                    null,
                    null,
                    Telephony.Sms.Inbox.DEFAULT_SORT_ORDER); // Default sort order
        } else {
            cur = cr.query(Telephony.Sms.Sent.CONTENT_URI, // Official CONTENT_URI from docs
                    new String[]{Telephony.Sms.Sent.BODY}, // Select body text
                    null,
                    null,
                    Telephony.Sms.Sent.DEFAULT_SORT_ORDER); // Default sort order
        }

        int totalSMS = cur.getCount();

        if (cur.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {
                int inx = 0;
                try {
                    inx = cur.getColumnIndexOrThrow("address");
                    lstSms.add("_MSG_address:" + cur.getString(inx));
                } catch (IllegalArgumentException e) { lstSms.add("???"); /* column doesn't exist */ }

                try {
                    inx = cur.getColumnIndexOrThrow("body");
                    lstSms.add("_MSG_body:" + cur.getString(inx).replace("\n", "").replace("\r", ""));
                } catch (IllegalArgumentException e) { lstSms.add("???"); /* column doesn't exist */ }

                try {
                    inx = cur.getColumnIndexOrThrow("date");

                    java.util.Date date = new java.util.Date();
                    long unix_time = cur.getLong(inx);
                    date.setTime((long)unix_time*1000);

                    SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                    // give a timezone reference for formatting
                    sdf.setTimeZone(java.util.TimeZone.getDefault());
                    String formattedDate = sdf.format(date);

                    lstSms.add("_MSG_date:" + formattedDate.toString());
                } catch (IllegalArgumentException e) { lstSms.add("???"); /* column doesn't exist */ }
                cur.moveToNext();
            }
        } else {
            // No SMS in Inbox or Outbox
        }
        cur.close();

        return lstSms;
    }

    public List<String> getAllSmsAPIBefore19(boolean inbox) {// inbox == false if outbox
        List<String> lstSms = new ArrayList<String>();

        Uri uriSMSURI = null;
        if (inbox)  uriSMSURI = Uri.parse("content://sms/inbox");
        else        uriSMSURI = Uri.parse("content://sms/sent");
        Cursor cur = this.mainAct.getContentResolver().query(uriSMSURI, null, null, null,null);

        if (cur.moveToFirst()) {
            do {
                int inx = 0;
                try {
                    inx = cur.getColumnIndexOrThrow("address");
                    lstSms.add("_MSG_address:" + cur.getString(inx));
                } catch (IllegalArgumentException e) { lstSms.add("???"); /* column doesn't exist */ }

                try {
                    inx = cur.getColumnIndexOrThrow("body");
                    lstSms.add("_MSG_body:" + cur.getString(inx).replace("\n", "").replace("\r", ""));
                } catch (IllegalArgumentException e) { lstSms.add("???"); /* column doesn't exist */ }

                try {
                    inx = cur.getColumnIndexOrThrow("date");

                    java.util.Date date = new java.util.Date();
                    long unix_time = cur.getLong(inx);
                    date.setTime((long)unix_time);

                    SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                    // give a timezone reference for formatting
                    sdf.setTimeZone(java.util.TimeZone.getDefault());
                    String formattedDate = sdf.format(date);

                    lstSms.add("_MSG_date:" + formattedDate.toString());
                } catch (IllegalArgumentException e) { lstSms.add("???"); /* column doesn't exist */ }
            } while (cur.moveToNext());
        } else {
            // No SMS in Inbox or Outbox
        }
        cur.close();

        return lstSms;
    }



}
